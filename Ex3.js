/*
INPUT:
Nhập vào 3 số bất kì
TODO: kiểm tra từng số 
là số chẵn thì lấy số đó % 2 = 0, còn lại là số lẻ
OUTPUT:
In ra số lượng số chẵn, lẻ
*/ 
function display(){
    var n1 = document.getElementById("so1").value*1;
    var n2 = document.getElementById("so2").value*1;
    var n3 = document.getElementById("so3").value*1;
    var soChan = 0;
    var soLe = 0;
    if ( n1 % 2 == 0){
        soChan++; 
    } else {
        soLe++;
    }
    if ( n2 % 2 == 0){
        soChan++; 
    } else {
        soLe++;
    }
    if ( n3 % 2 == 0){
        soChan++; 
    } else {
        soLe++;
    }
    console.log(soChan);
    console.log(soLe);
    document.getElementById("result").innerHTML = `<p> Có ${soChan} số chẵn, và ${soLe} số lẻ`;


}