/*
INPUT: Lập danh sách 4 thành viên
TODO: Sử dụng câu lệnh If-else và gán lời chào cho từng thành viên
OUTPUT: Hiển thị lời chào tương ứng cho từng thành viên
*/ 
function send(){
    var greeting;
    var m = document.getElementById("member");
    var member = m.options[m.selectedIndex].value;
    // var Mom = document.getElementById("Mom").value;
    // var Sis = document.getElementById("Sis").value;
    // var Bro = document.getElementById("Bro").value;

    
    if ( member == "Dad"){
        greeting = "Xin chào Bố !";
    }
    else if ( member == "Mom"){
        greeting = "Xin chào Mẹ !";
    }
    else if ( member == "Sis"){
        greeting = "Xin chào Chị gái !";
    }
    if ( member == "Bro"){
        greeting = "Xin chào Anh trai !";
    }
    document.getElementById("result").innerHTML = `<p> Lời chào: ${greeting}`;

}