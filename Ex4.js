/*
INPUT: Nhập vào 3 cạnh của tam giác
TODO:  Kiểm tra kiểu tam giác
Nếu 3 cạnh bằng nhau => tam giác đều
Nếu 2 trong 3 cạnh bằng nhau => tam giác cân
Nếu 1 cạnh bình phương bằng tổng của 2 cạnh kia bình phương theo định lí Pytago => tam giác vuông
Còn lại là trường hợp khác
OUTPUT:
Hiển thị kiểu tam giác
*/ 
function guess(){
    var n1 = document.getElementById("canh1").value*1;
    var n2 = document.getElementById("canh2").value*1;
    var n3 = document.getElementById("canh3").value*1;
    var result;

    if (n1 == n2 && n2 == n3){
        result = "Đây là tam giác đều";
    } else if (n1 == n2 || n1 == n3 || n2 == n3){
        result = "Đây là tam giác cân";
    }
    else if (n3 == Math.sqrt(n1*2 + n2*2) || n1 == Math.sqrt(n2*2 + n3*2) || n2 == Math.sqrt(n1*2 + n3*2) ){
        result = "Đây là tam giác vuông"

    }
    else {
        result="Trường hợp khác";
    }
    document.getElementById("result").innerHTML = `<p> ${result}</p>`
}