/*
INPUT: Nhập vào 3 số nguyên
n1 = ?
n2 = ?
n3 = ?
TODO: Sử dụng các lệnh If else lồng nhau
Đầu tiên, so sánh 2 số n1 > n2, có 3 trường hợp:
+ n2 > n3 => n3 < n2 < n1
+ n1 > n3 => n2 < n3 < n1
+ n3 > n1 > n2 
Tiếp theo, so sánh 2 số n2 > n3, có 2 trường hợp:
+ n3 < n1 < n2
+ n1 < n3 < n2
Và trường hợp còn lại n1 < n2 < n3.
OUTPUT:
=> Xuất ra thứ tự 3 số nguyên
*/ 
function printAscending(){
    var n1 = document.getElementById("so1").value*1;
    var n2 = document.getElementById("so2").value*1;
    var n3 = document.getElementById("so3").value*1;
    var daySo;
    if (n1 > n2){
        if ( n2 > n3){
            daySo = n3 + "<" + n2 + "<" + n1;
        } else if( n1 > n3 ){
            daySo = n2 + "<" + n3 + "<" + n1;
        } else{
            daySo = n2 + "<" + n1 + "<" + n3;
        }
    }
    else if (n2 > n3){
        if ( n1 > n3 ){
            daySo = n3 + "<" + n1 + "<" + n2;
        } else {
            daySo = n1 + "<" + n3 + "<" + n2;
        }
    }
    else {
        daySo = n1 + "<" + n2 + "<" + n3;
    }
    console.log(daySo);
    document.getElementById("result").innerHTML = `<p> Thứ tự tăng dần là: ${daySo} `;

}